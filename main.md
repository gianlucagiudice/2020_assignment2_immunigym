
# Table of Contents

1.  [Introduzione](#org8ca94e0)
    1.  [Assignement](#org07e56a7)
    2.  [Applicazione](#org9613962)
2.  [Lista degli stakeholders](#org53000f2)
    1.  [Responsabile della palestra (forse solo "Palestra")](#orgf08a5b3)
    2.  [Team di sviluppo](#org09cb7db)
    3.  [Utenti](#org7f86c83)
        1.  [Iscritti alla palestra](#org4555204)
        2.  [Non iscritti alla palestra](#org95e14be)
    4.  [Istruttori](#orgfda2f81)
    5.  [Responsabili sanitari](#org18dd7e4)
3.  [Tecniche di elicitazione](#orgd85bbae)
    1.  [Background study](#org8f80e01)
    2.  [Intervista](#org1b864c6)
    3.  [Questionario](#orga7428aa)
        1.  [Informazioni generiche](#orgef78276)
        2.  [Questionario per utenti iscritti](#org00ca259)
        3.  [Questionario per potenziali utenti](#orga3e884e)
    4.  [Mock up o creazione di scenari](#orgd0cd9a3)


<a id="org8ca94e0"></a>

# Introduzione


<a id="org07e56a7"></a>

## Assignement

Il documento è salvato sul repository GitLab al link:
<https://gitlab.com/gianlucagiudice/2020_assignment2_immunigym>

Membri del gruppo:

-   Gianluca Giudice - 830694
-   Marco Grassi - 829664


<a id="org9613962"></a>

## Applicazione

**ImmuniGym** è un'applicazione che si occupa delle prenotazioni degli
ingressi di una palestra.

A causa delle normative anticovid, le palestre sono tenute a
consentire l'accesso degli iscritti in un numero
limitato. L'obbiettivo del sistema è gestire le prenotazioni con lo
scopo di contingentare le entrate e rispettare i limiti sul numero
di ingressi in una determinata fascia oraria, così da evitare
affollamento.

Le diverse funzionalità da implementare vengono introdotte e
analizzate da parte degli stakeholders nelle diverse sezioni di
questo documento.

Il documento si divide in 4 parti:

1.  **Lista dei soggetti interessati** all’applicazione (stakeholders).
2.  **Lista delle attività** da svolgere con lo scopo di scoprire i
    requisiti da soddisfare.
3.  **Lista delle domande del questionario** che verranno sottoposte ad
    alcuni stakeholders.
4.  **Intervista** sottoposta a stakeholders interessati, una strategia
    differente rispetto al questionario, con lo scopo di scoprire
    ulteriori requisiti da soddisfare.


<a id="org53000f2"></a>

# Lista degli stakeholders


<a id="orgf08a5b3"></a>

## Responsabile della palestra (forse solo "Palestra")

È importante coinvolgere il responsabile della palestra in quanto
serviranno le informazioni fino a quel punto gestite dal
system-as-is.

Nel particolare sarà necessario conoscere:

-   Le informazioni relative agli utenti.
-   Come è organizzata la palestra per quanto riguarda corsi e orari.


<a id="org09cb7db"></a>

## Team di sviluppo

Il team di sviluppo fa parte degli stakeholders e sono una
componentne cruciale per la creazione del progetto, in quanto
saranno proprio questi a progettare e sviluppare effettivamente il
sistema. Per questo motivo devono essere costantemente aggiornati
sui nuovi requisiti richiesti, così da valutarne la fattibilità, i
costi, e i tempi.

Nel caso in cui si riscontrino delle criticità nello sviluppo di
qualche specifica di un requisito, sarà necessario organizzare
sessioni di gruppo così da cercare di risolvere le problematiche
emerse.


<a id="org7f86c83"></a>

## Utenti


<a id="org4555204"></a>

### Iscritti alla palestra

Gli utenti già iscritti sono i diretti interessati e i principiali
fruitori del sistema. Rientrano quindi tra gli stakeholders e
avranno molta influenza sullo sviluppo del sistema.

Essendo gli utenti diretti del sistema, sarà necessario sottoporre
a loro dei questionari così da scoprire i requisiti e le
funzionalità che dovranno essere presenti.


<a id="org95e14be"></a>

### Non iscritti alla palestra

Per poter allargare il bacino di iscritti alla palestra sarà utile
coinvolgere utenti non ancora iscritti e cercare di capire quali
funzionalità o servizi potrebbero convincerli ad iscriversi.


<a id="orgfda2f81"></a>

## Istruttori


<a id="org18dd7e4"></a>

## Responsabili sanitari

Lo scopo principale del sistema è gestire le entrate in modo da
evitare l'affollamento in rispetto delle norme igenico
sanitarie. Per questo motivo è necessario coinvolgere esperti di
dominio dell'ambito sanitario così da soddisfare questi requisiti.


<a id="orgd85bbae"></a>

# Tecniche di elicitazione

Grafico con susseguirsi delle attività nell'ordine:

1.  Background study
2.  Questionario utenti
3.  Intervista al resposabile sanitario
4.  Mockup e prova del mockup


<a id="org8f80e01"></a>

## Background study

-   **Perchè un background study? :** Viene svolta una fase preliminare
    di ricerca per poter meglio identificare il dominio applicativo,
    analizzare sistemi as-is concorrenti e sviluppare una conoscenza
    di fondamento. Per esempio si identificano i corsi offerti dalla
    palestra e i vari macchinari in loro possesso.
    Studio delle normative di sicurezza per fronteggiare al Covid.
-   **Stakeholders coinvolti:** Nessuno
-   **Requisiti attesi:** Identificare le possibili dinamiche di
    utilizzo fisico della palestra che informeranno il possibile
    sviluppo di requisiti da soddisfare con l'applicazione.


<a id="org1b864c6"></a>

## Intervista

-   **Stakeholder coinvolti:** Responsabile sanitario
-   **Perchè?:** Il rispettare le norme di sicurezza è l'elemento
    cardine di questo progetto, se questo elemento viene meno di
    conseguenza tutte le funzionalità previste non potranno essere
    erogate. È quindi bene accertarsi che gli accorgimenti presi per
    via del Covid siano individuati con chiarezza.
-   **Requisiti attesi:** Una conferma che le misure di sicurezza
    adottate sono sufficenti ed attuabili. Gran parte della
    possibilità di attuazione di questo piano sono legati al giudizio
    di attuabilità del responsabile sanitario.


<a id="orga7428aa"></a>

## Questionario

Si assume che la palestra sia in possesso degli indirizzi email
degli utenti iscritti.

-   **Stakeholder coinvolti:** Utenti iscritti, Potenziali utenti
-   **Perchè:** È importante chiedere ai diretti interessati, cioè
    l'utente che tipo di funzionalità si aspettano. Inoltre tramite
    alcune domande del questionario sarà possibile verificare la
    presenza di interesse per attivita da remoto come classi o corsi.
-   **Requisiti attesi:** Una serie di funzionalità che l'applicazione
    dovrà fornire.


<a id="orgef78276"></a>

### Informazioni generiche


<a id="org00ca259"></a>

### Questionario per utenti iscritti


<a id="orga3e884e"></a>

### Questionario per potenziali utenti


<a id="orgd0cd9a3"></a>

## Mock up o creazione di scenari

